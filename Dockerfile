FROM alpine:3.8
WORKDIR /app
RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/v3.7/main/ nodejs=8.9.3-r1
COPY . ./
RUN npm install
EXPOSE 3000
CMD [ "node", "server.js" ]
